import pytest
from pathlib import Path
from lattice_converter.converter import main as convert

TEST_DIR = Path(__file__).parent

def test_run():
    convert(code_from='SAD', code_to='MADX', inputfile=TEST_DIR/'SAD_Lattice'/'lattice_for_conversion.sad', outputfile='./MAD_lattice_converted.mad')

@pytest.mark.xfail(raises=NotImplementedError)
def test_not_implemented():
    convert(code_from='SAD', code_to='ELEGANT', inputfile=TEST_DIR/'SAD_Lattice'/'lattice_for_conversion.sad', outputfile='./MAD_lattice_converted.mad')

@pytest.mark.xfail(raises=ValueError)
def test_same_code_in_out():
    convert(code_from='SAD', code_to='SAD', inputfile=TEST_DIR/'SAD_Lattice'/'lattice_for_conversion.sad', outputfile='./MAD_lattice_converted.mad')

@pytest.mark.xfail(raises=OSError)
def test_invalid_input():
    convert(code_from='SAD', code_to='MADX', inputfile='a', outputfile='./MAD_lattice_converted.mad')
