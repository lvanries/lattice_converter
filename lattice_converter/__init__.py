"""
lattice_converter
~~~~~~~~~~~~~~~~


"""
__title__ = "lattice_converter"
__description__ = "Python script to translate accelerator lattices between different codes."
__url__ = "https://gitlab.cern.ch/lvanries/lattice_converter"
__version__ = "1.0.0"
__author__ = "lvanries"
__author_email__ = "lvanries@cern.ch"
__license__ = "MIT"

__all__ = [__version__]