"""
Lattice_convert
--------------------------------------------------------------------------------

Using a special formatted outputfile of a given accelerator code, this script 
can translate it to use with another accelerator code of choice.

*--Required--*
- **from** *(str)*: Format of the input sequence.
- **to** *(str)*: Format of the output sequence.
- **inputfile** *(str)*: Path to the input file.
- **outputfile** *(str)*: Path to the output file.

*--Optional--*

"""
from pathlib import Path
from generic_parser import EntryPointParameters, entrypoint
from lattice_converter import sad, madx, elegant
from lattice_converter.tools import PathOrStr

# Constants --------------------------------------------------------------------

CODES = dict(
    SAD=sad,
    MADX=madx,
    ELEGANT=elegant,
)
OPTIONS = set(CODES.keys())

# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="code_from",
        type=str,
        required=True,
        choices=OPTIONS,
        help=f"Format of the input sequence. Options are {OPTIONS}",
    )
    params.add_parameter(
        name="code_to",
        type=str,
        required=True,
        choices=OPTIONS,
        help=f"Format of the output sequence. Options are {OPTIONS}",
    )
    params.add_parameter(
        name="inputfile",
        type=PathOrStr,
        required=True,
        help="Path to the input file.",
    )
    params.add_parameter(
        name="outputfile",
        type=PathOrStr,
        required=True,
        help="Path to the output file.",
    )
     
    return params


# Entrypoint -------------------------------------------------------------------
@entrypoint(get_params(), strict=True)
def main(opt):
    opt = _check_opts(opt)
    df, _ = CODES[opt.code_from].reader(opt.inputfile)
    CODES[opt.code_to].writer(df, opt.outputfile)

    return df    
    

def _check_opts(opt):

    if opt.code_from == opt.code_to :
        raise ValueError('Input and Output format are the same. Nothing to do here.')

    if Path(opt.inputfile).is_file():    
        opt["inputfile"]=Path(opt.inputfile)
    else:
        raise OSError('Inputfile path appears to be invalid.')
    
    opt = _convert_str_to_path(opt, 'outputfile')

    return opt


def _convert_str_to_path(opt, file):
    if opt[file]!=None:
        opt[file]=Path(opt[file])
    return opt


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()
