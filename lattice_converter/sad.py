import pandas as pd
import numpy as np


def reader(filename):
    #Reads the SAD lattice and puts it in a data frame 
    
    #Make sure beamline file is in the right csv format
    beamline  = pd.read_csv(filename,sep="\t")
    
    #remove end marker
    beamline = beamline.loc[beamline['ELEMENT'] != "$$$"]
    
    beamline = sol_regions(beamline)
    
    #identify unique elements
    elements  = np.unique(beamline['ELEMENT'].values)
    
    return beamline, elements


def sol_regions(beamline):

    epfac = 0.299792458

    #Define a column KSOL which has the value of ksol when located inside solenoid
    beamline["KSOL"] = 0
    beamline["NSLICE"] = 0
    beamline["BZ_ELEMENT"] = ''
    
    ksol = 0
    bz_element = ''
    
    for idx in beamline.index:
                
        if ksol != 0:
            beamline.loc[idx,"KSOL"] = ksol
            beamline.loc[idx,"BZ_ELEMENT"] = bz_element
    
        if beamline.loc[idx,"TYPENAME"] == "SOL" and beamline.loc[idx,"DIR"] == 1:
            ksol = epfac * beamline.loc[idx,"BZ"] /beamline.loc[idx,"MOMENTUM"]
            bz_element = beamline.loc[idx,"ELEMENT"]
            
    ksol = 0
    
    for idx in reversed(beamline.index):
        if ksol != 0:
            beamline.loc[idx,"KSOL"] = ksol
            beamline.loc[idx,"BZ_ELEMENT"] = bz_element
    
        if beamline.loc[idx,"TYPENAME"] == "SOL" and beamline.loc[idx,"DIR"] == -1:
            ksol = - epfac * beamline.loc[idx,"BZ"] /beamline.loc[idx,"MOMENTUM"]
            bz_element = beamline.loc[idx,"ELEMENT"]
            
    return beamline


def writer(df, filename):
    raise NotImplementedError('SAD writer has not yet been implemented.')