from pathlib import Path
from generic_parser.entry_datatypes import get_instance_faker_meta

# Helper Functions -------------------------------------------------------------
class PathOrStr(metaclass=get_instance_faker_meta(Path, str)):
    """A class that behaves like a Path when possible, otherwise like a string."""
    def __new__(cls, value):
        if value is None:
            return None

        if isinstance(value, str):
            value = value.strip("\'\"")  # behavior like dict-parser, IMPORTANT FOR EVERY STRING-FAKER
        return Path(value)