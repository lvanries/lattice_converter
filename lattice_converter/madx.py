import tfs

def reader(filename):
    tfs_df = tfs.read(filename)

    df = _convert_to_common_standard(tfs_df)

    return df
    

def _convert_to_common_standard(df):

    return df


def writer(beamline, elements, filename):
    
    #Generate information about the parent elements in the sequence in MADX format
    #Note beamline is returned by the same function because information is added to the dataframe
    element_lengths, element_strengths, element_definitions, beamline = madx_element_writer(beamline, elements)
    
    #Generate derived elements and beamline for MADX
    madx_elements, madx_beamline = madx_beamline_writer(beamline)
    
    #Generate the matching scripts required for the solenoid
    madx_matching = solenoid_matching(beamline)
    
    #Takes the generated strings and put them in appropriate lattice, optics and matching files
    madx_file_writer(filename,element_lengths, element_strengths, element_definitions,madx_elements, madx_beamline,madx_matching)
    
    return


def madx_element_writer(beamline, elements):
    
    #Initiate empty strings
    element_lengths = ""
    element_strengths = ""
    element_definitions = ""
 
    #Loop over all elements
    for element in elements:
        
        #Get all the beamline elements derived from a given element 
        beamline_elements = beamline.loc[beamline['ELEMENT'] == element]
        
        if len(np.unique(beamline_elements['KSOL'].values)) > 1:
            print("different solenoid fields in the same element")
            print(beamline_elements)
            

        #Drift
        if beamline_elements["TYPENAME"].values[0] == 'DRIFT':
            
            #Case when we are in a solenoid region. Drift = solenoid
            if beamline_elements["KSOL"].values[0] == 0:
            
                element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
                element_definitions = element_definitions + element + " : DRIFT, L := L_"+ element + ";\n"
            
            #If not it is a simple drift
            else: 
            
                element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
                element_definitions = element_definitions + element + " : SOLENOID, L := L_"+ element + ", KS := KS_"+ beamline_elements["BZ_ELEMENT"].values[0] +";\n"
        
        #Bending dipole
        elif beamline_elements["TYPENAME"].values[0] == 'BEND':
            
            #Placeholder since SKEKB contain zero length and strength dipoles... Just implement as marker
            if beamline_elements["L"].values[0] == 0.0 and beamline_elements["ANGLE"].values[0] == 0.0:
            
                
                element_definitions = element_definitions + element + " : MARKER" 
            
            #We use the SBEND as "default" dipole as per SAD convention
            elif beamline_elements["E1"].values[0] == 0.0 and beamline_elements["E2"].values[0] == 0.0:
            
                element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
                element_strengths = element_strengths + "ANGLE_" + element + " = " + str(beamline_elements["ANGLE"].values[0]) + ";\n"
                element_definitions = element_definitions + element + " : SBEND, L := L_"+ element + ", ANGLE := ANGLE_"+ element 
            #MADX E1 and E2 are NOT normalised with bend angle, hence we need to multiply
            #Note, we do not use RBENDs because in MADX Length refers to the length of the RBEND NOT the pathlength. Sticking with SAD convention.
            else:
                element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
                element_strengths = element_strengths + "ANGLE_" + element + " = " + str(beamline_elements["ANGLE"].values[0]) + ";\n"
                element_definitions = element_definitions + element + " : SBEND, L := L_"+ element + ", ANGLE := ANGLE_"+ element + ", E1 := "+str(beamline_elements["E1"].values[0])+" * ANGLE_"+ element + ", E2 := "+str(beamline_elements["E2"].values[0])+" * ANGLE_"+ element
            
            #Standard command in case the element is rotated.            
            if beamline_elements["ROTATE"].values[0] == 0 or beamline_elements["ANGLE"].values[0] == 0.0:
                element_definitions = element_definitions + ";\n"
            else:
                element_definitions = element_definitions + ", TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"
        
        #Quadruopole
        elif beamline_elements["TYPENAME"].values[0] == 'QUAD':
        
            element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
            element_strengths = element_strengths + "K1L_" + element + " = " + str(beamline_elements["K1L"].values[0]) + ";\n"
            element_definitions = element_definitions + element + " : QUADRUPOLE, L := L_"+ element + ", K1 := K1L_"+ element + "/L_"+ element 
            #Standard command in case the element is rotated.
            if beamline_elements["ROTATE"].values[0] == 0:
                element_definitions = element_definitions + ";\n"
            else:
                element_definitions = element_definitions + ", TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"
        
        #Sextupole        
        elif beamline_elements["TYPENAME"].values[0] == 'SEXT':
        
            element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
            element_strengths = element_strengths + "K2L_" + element + " = " + str(beamline_elements["K2L"].values[0]) + ";\n"
            element_definitions = element_definitions + element + " : SEXTUPOLE, L := L_"+ element + ", K2 := K2L_"+ element + "/L_"+ element 
            
            #Standard command in case the element is rotated.
            if beamline_elements["ROTATE"].values[0] == 0:
                element_definitions = element_definitions + ";\n"
            else:
                element_definitions = element_definitions + ", TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"   
        
        #Octupole...        
        elif beamline_elements["TYPENAME"].values[0] == 'OCT':
        
            element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
            element_strengths = element_strengths + "K3L_" + element + " = " + str(beamline_elements["K3L"].values[0]) + ";\n"
            element_definitions = element_definitions + element + " : OCTUPOLE, L := L_"+ element + ", K3 := K3L_"+ element + "/L_"+ element 
            
            #Standard command in case the element is rotated.
            if beamline_elements["ROTATE"].values[0] == 0:
                element_definitions = element_definitions + ";\n"
            else:
                element_definitions = element_definitions + ", TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"
        
        #"Thick" Multipole
        elif beamline_elements["TYPENAME"].values[0] == 'MULT':
            
            #EPS based on EOS in SAD - determines number of slices depending on multipole strengths
            EPS = 0.001
            nslice = 1
            
            element_strengths = element_strengths + "K0L_" + element + " = " + str(beamline_elements["K0L"].values[0]) + ";\n"
            element_strengths = element_strengths + "K0SL_" + element + " = " + str(beamline_elements["K0SL"].values[0]) + ";\n"
            
            #EPS algorithm, obtained from an Email by Oide-San...
            for i in range(1,21):
                nslice = max([nslice,1 + np.sqrt(0.05**(i-1) * np.sqrt(float(beamline_elements["K"+str(i)+"L"].values[0])**2 + float(beamline_elements["K"+str(i)+"SL"].values[0])**2) * beamline_elements["L"].values[0] / (6 * np.math.factorial(i-1) * EPS) )])
                element_strengths = element_strengths + "K"+str(i)+"L_" + element + " = " + str(beamline_elements["K"+str(i)+"L"].values[0]) + ";\n"
                element_strengths = element_strengths + "K"+str(i)+"SL_" + element + " = " + str(beamline_elements["K"+str(i)+"SL"].values[0]) + ";\n"
            
            #Store number of slices in the dataframe as this will be important for the beamline generation 
            nslice = int(np.floor(nslice))
            beamline["NSLICE"].loc[beamline['ELEMENT'] == element] = int(np.floor(nslice))
            
            
            element_lengths = element_lengths + "L_" + element + " = " + str(beamline_elements["L"].values[0]) + ";\n"
            
            #First the normal case when we are outside the solenoid
            if beamline_elements["KSOL"].values[0] == 0:
            
                #Generate all the slices 
                for i in range(1,nslice+1):
                    element_definitions = element_definitions + element + ".DRIFT.L." + str(i) + " : DRIFT, L := L_"+ element + "/(" + str(nslice) + "*2);\n"    
                    
                    element_definitions = element_definitions + element + ".SLICE." + str(i) + " : MULTIPOLE, LRAD := L_"+ element + "/" + str(nslice) + ", KNL := {0"
                    #All the multipoles and skews
                    for j in range(1,21):
                        element_definitions = element_definitions + ", K"+str(j)+"L_" + element + "/" + str(nslice)   
                    element_definitions = element_definitions + "}, KSL := {0"
                    for j in range(1,21):
                        element_definitions = element_definitions + ", K"+str(j)+"SL_" + element + "/" + str(nslice)  
                    
                    #Standard command in case the element is rotated.
                    if beamline_elements["ROTATE"].values[0] == 0:
                        element_definitions = element_definitions + "};\n"
                    else:
                        element_definitions = element_definitions + "}, TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"
                    
                    #k0 implemented as kick since otherwise it changes geometry
                    element_definitions = element_definitions + element + ".KICK." + str(i) + " : KICKER, L=0, HKICK := K0L_" + element + "/" + str(nslice) + ", VKICK := K0SL_" + element + "/" + str(nslice)
                    if beamline_elements["ROTATE"].values[0] == 0:
                        element_definitions = element_definitions + ";\n"
                    else:
                        element_definitions = element_definitions + ", TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"
                    
                    element_definitions = element_definitions + element + ".DRIFT.R." + str(i) + " : DRIFT, L := L_"+ element + "/(" + str(nslice) + "*2);\n"        
            
            #Same as above but drifts - > solenoid
            else:
            
                for i in range(1,nslice+1):
                    element_definitions = element_definitions + element + ".DRIFT.L." + str(i) + " : SOLENOID, L := L_"+ element + "/(" + str(nslice) + "*2), KS := KS_"+ beamline_elements["BZ_ELEMENT"].values[0] +";\n"        
                    
                    element_definitions = element_definitions + element + ".SLICE." + str(i) + " : MULTIPOLE, LRAD := L_"+ element + "/" + str(nslice) + ", KNL := {0"
                    for j in range(1,21):
                        element_definitions = element_definitions + ", K"+str(j)+"L_" + element + "/" + str(nslice)   
                    element_definitions = element_definitions + "}, KSL := {0"
                    for j in range(1,21):
                        element_definitions = element_definitions + ", K"+str(j)+"SL_" + element + "/" + str(nslice)  
                    if beamline_elements["ROTATE"].values[0] == 0:
                        element_definitions = element_definitions + "};\n"
                    else:
                        element_definitions = element_definitions + "}, TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"
                    
                    element_definitions = element_definitions + element + ".KICK." + str(i) + " : KICKER, L=0, HKICK := K0L_" + element + "/" + str(nslice) + ", VKICK := K0SL_" + element + "/" + str(nslice)
                    if beamline_elements["ROTATE"].values[0] == 0:
                        element_definitions = element_definitions + ";\n"
                    else:
                        element_definitions = element_definitions + ", TILT = "+str(beamline_elements["ROTATE"].values[0])+";\n"
                    
                    element_definitions = element_definitions + element + ".DRIFT.R." + str(i) + " : SOLENOID, L := L_"+ element + "/(" + str(nslice) + "*2), KS := KS_"+ beamline_elements["BZ_ELEMENT"].values[0] +";\n"        
            

            
        #Solenoid markers    
        elif beamline_elements["TYPENAME"].values[0] == 'SOL':
            
            #Define solenoid fields for elements that inherit this (drifts in this region)
            element_strengths = element_strengths + "BZ_" + element + " := " + str(beamline_elements["DIR"].values[0]) + " * (" + str(beamline_elements["BZ"].values[0]) + ");\n"
            element_strengths = element_strengths + "KS_" + element + " := " + "BZ_" + element + " * 0.299792458 / " + str(beamline_elements["MOMENTUM"].values[0]/1E9) + ";\n"
            
            
            #Rotations and translations needed for the geometry redefintion
            element_strengths = element_strengths + "THETA_" + element + " = " + str(- 1.0 * beamline_elements["CHI1"].values[0]) + ";\n"
            
            element_strengths = element_strengths + "DX_" + element + " = " + str(beamline_elements["DIR"].values[0]*beamline_elements["DX"].values[0]) + ";\n"
            element_strengths = element_strengths + "DY_" + element + " = (-1)*(" + str(beamline_elements["DIR"].values[0]*beamline_elements["DY"].values[0]) + ");\n"
            element_strengths = element_strengths + "DS_" + element + " = (-1)*(" + str(beamline_elements["DIR"].values[0]*beamline_elements["DZ"].values[0]) + ");\n"
            
            
            element_definitions = element_definitions + element + "_DRIFT : DRIFT, L := ABS(DS_"+ element +");\n"
            element_definitions = element_definitions + element + "_TRANS : TRANSLATION, DX := DX_"+ element + ", DY := DY_"+ element + ", DS := DS_"+ element +";\n"
            element_definitions = element_definitions + element + "_ROT : YROTATION, ANGLE := THETA_"+ element +";\n"
            
        #Cavity - set lag to 0.5 as this is a good first guess in most cases...                
        elif beamline_elements["TYPENAME"].values[0] == 'CAVI':
        
            element_strengths = element_strengths + "VOLT_" + element + " = " + str(beamline_elements["VOLT"].values[0]/1000000) + ";\n"
            element_strengths = element_strengths + "FREQ_" + element + " = " + str(beamline_elements["FREQ"].values[0]/1000000) + ";\n"
            
            element_definitions = element_definitions + element + " : RFCAVITY, VOLT := VOLT_"+ element + ", FREQ := FREQ_"+ element +", LAG = 0.5;\n"
        
        #Markers, BPMS also SAD "Aperture" elements since there is no MADX equivalent...
        elif beamline_elements["TYPENAME"].values[0] == 'MARK' or beamline_elements["TYPENAME"].values[0] == 'MONI' or beamline_elements["TYPENAME"].values[0] == 'BEAMBEAM' or beamline_elements["TYPENAME"].values[0] == 'APERT':
        
            element_definitions = element_definitions + element + " : MARKER;\n"
            
        else:
            print('Unknown element type')
            print(beamline_elements["TYPENAME"].values[0])
            element_definitions = element_definitions + element + " : MARKER;\n"
            
    return element_lengths, element_strengths, element_definitions, beamline
            
def madx_beamline_writer(beamline):
    #Here we define the strings for the individual (derived) elements and the actual beamline for lattice file
    
    madx_elements = ''
    #"Line" convention for readability but also to conserve infor 
    madx_beamline = 'RING: LINE = (\t'
    
    #loop over beamline
    for index, row in beamline.iterrows():
        #"Thick" multipoles defined as small line, later implanted in the line
        if row["TYPENAME"] == "MULT":
        
            madx_elements = madx_elements + row["NAME"] + " : LINE = ("
            
            for i in range(1,row["NSLICE"]+1):
                madx_elements = madx_elements + row["ELEMENT"] + ".DRIFT.L." + str(i) + ","+ row["ELEMENT"] + ".SLICE." + str(i) + "," + row["ELEMENT"] + ".KICK." + str(i) + "," + row["ELEMENT"] + ".DRIFT.R." + str(i) + ","
                
            #madx_elements = madx_elements[:len(madx_beamline) - 1]
            madx_elements = madx_elements + ");\n"
        
        #Solenoid translation and rotation package defined as small line, later implanted in the line
        elif row["TYPENAME"] == "SOL":
        
            madx_elements = madx_elements + row["NAME"] + ": LINE = ("+ row["ELEMENT"] + "_TRANS, "+ row["ELEMENT"] + "_DRIFT, "+ row["ELEMENT"] + "_ROT);\n"
            
        else:
            madx_elements = madx_elements + row["NAME"] + ": " + row["ELEMENT"] + ";\n"
        
        if index % 10 == 9:
            madx_beamline = madx_beamline + "\n\t\t"   
            
        madx_beamline = madx_beamline + row["NAME"] + ", "
        
    madx_beamline = madx_beamline + ");\n"
    
    return madx_elements, madx_beamline
           
def solenoid_matching(beamline):
    #The matching scripts for the solenoid region
    start = beamline.loc[0]
    solenoids = beamline.loc[beamline['BOUND'] == 1]
    solenoids = solenoids.drop_duplicates(subset=['ELEMENT'])
    
    madx_matching = ''
    
    #First match as a line
    #Markers come in pairs :)
    for i in range(int(len(solenoids)/2)):
    
        madx_matching = madx_matching + "MATCH, SEQUENCE = RING, ALFX = "+str(start.ALFX)+", ALFY = "+str(start.ALFY)+", BETX = "+str(start.BETX)+", BETY = "+str(start.BETY)+";\n"
        
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i]+"_ROT, PX = "+str(solenoids["CHI1"].iloc[2*i])+";\n"
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i+1]+"_ROT, PX = 0;\n"
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i+1]+"_ROT, X = 0;\n"
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i+1]+"_ROT, T = 0;\n"
        
        madx_matching = madx_matching + "\tVARY, NAME = THETA_"+solenoids["ELEMENT"].iloc[2*i]+";\n"
        madx_matching = madx_matching + "\tVARY, NAME = DX_"+solenoids["ELEMENT"].iloc[2*i+1]+";\n"
        madx_matching = madx_matching + "\tVARY, NAME = DS_"+solenoids["ELEMENT"].iloc[2*i+1]+";\n"
        madx_matching = madx_matching + "\tVARY, NAME = THETA_"+solenoids["ELEMENT"].iloc[2*i+1]+";\n"

        madx_matching = madx_matching + "\tLMDIF, CALLS=1000, TOLERANCE=1.0E-21;\n"
        madx_matching = madx_matching + "ENDMATCH;\n\n\n"
        
    #Match closed orbit like in SAD
    for i in range(int(len(solenoids)/2)):
    
        madx_matching = madx_matching + "MATCH, SEQUENCE = RING;\n"
        
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i]+"_ROT, PX = "+str(solenoids["CHI1"].iloc[2*i])+";\n"
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i+1]+"_ROT, PX = 0;\n"
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i+1]+"_ROT, X = 0;\n"
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i+1]+"_ROT, Y = 0;\n"
        madx_matching = madx_matching + "\tCONSTRAINT, SEQUENCE = RING, RANGE = "+solenoids["ELEMENT"].iloc[2*i+1]+"_ROT, T = 0;\n"
        
        madx_matching = madx_matching + "\tVARY, NAME = THETA_"+solenoids["ELEMENT"].iloc[2*i]+";\n"
        madx_matching = madx_matching + "\tVARY, NAME = DX_"+solenoids["ELEMENT"].iloc[2*i+1]+";\n"
        madx_matching = madx_matching + "\tVARY, NAME = DY_"+solenoids["ELEMENT"].iloc[2*i+1]+";\n"
        madx_matching = madx_matching + "\tVARY, NAME = DS_"+solenoids["ELEMENT"].iloc[2*i+1]+";\n"
        madx_matching = madx_matching + "\tVARY, NAME = THETA_"+solenoids["ELEMENT"].iloc[2*i+1]+";\n"

        madx_matching = madx_matching + "\tLMDIF, CALLS=1000, TOLERANCE=1.0E-20;\n"
        madx_matching = madx_matching + "ENDMATCH;\n\n\n"
        
    return madx_matching    
    

           
def madx_file_writer(latticename,element_lengths, element_strengths, element_definitions,madx_elements, madx_beamline, madx_matching):
    #Writing the files...
    f = open(latticename+".str", "w")
    f.write(element_strengths)
    f.close()
    
    g = open(latticename+".seq", "w")
    g.write(element_lengths + "\n\n\n" + element_definitions + "\n\n\n" + madx_elements + "\n\n\n" +madx_beamline)
    g.close()
    
    h = open(latticename+"_matching.madx", "w")
    h.write(madx_matching)
    h.close()
    
    return