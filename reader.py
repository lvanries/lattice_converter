import pandas as pd
import numpy as np



def sad_reader(filename):
    #Reads the SAD lattice and puts it in a data frame 
    
    #Make sure beamline file is in the right csv format
    beamline  = pd.read_csv(filename,sep="\t")
    
    #remove end marker
    beamline = beamline.loc[beamline['ELEMENT'] != "$$$"]
    
    beamline = sad_sol_regions(beamline)
    
    
    #identify unique elements
    elements  = np.unique(beamline['ELEMENT'].values)
    
    return beamline, elements



def sad_sol_regions(beamline):

    epfac = 0.299792458

    #Define a column KSOL which has the value of ksol when located inside solenoid
    beamline["KSOL"] = 0
    
    ksol = 0
    
    for idx in beamline.index:
                
        if ksol != 0:
            beamline.loc[idx,"KSOL"] = ksol
    
        if beamline.loc[idx,"TYPENAME"] == "SOL" and beamline.loc[idx,"DIR"] == 1:
            ksol = epfac * beamline.loc[idx,"BZ"] /beamline.loc[idx,"MOMENTUM"]
            
    ksol = 0
    
    for idx in reversed(beamline.index):
        if ksol != 0:
            beamline.loc[idx,"KSOL"] = ksol
    
        if beamline.loc[idx,"TYPENAME"] == "SOL" and beamline.loc[idx,"DIR"] == -1:
            ksol = - epfac * beamline.loc[idx,"BZ"] /beamline.loc[idx,"MOMENTUM"]
            
    return beamline

beamline, elements  = sad_reader("./SAD_Lattice/lattice_for_conversion.sad")
print(beamline)


print(elements)
print(beamline.loc[beamline['ELEMENT'] == elements[1]])